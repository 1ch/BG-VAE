import torch
from torch.hub import load_state_dict_from_url
import copy
from VAE.models.registry import register_model
import VAE.models.common as cm
import VAE.models.practical.model as blk
import VAE.models.rd.model as lib



@register_model
def P_VAE(lmb_range=(64,8192), pretrained=False):
    cfg = dict()
    #to determine the Z block resblock type

    # maximum downsampling factor
    cfg['max_stride'] = 64
    # images used during training for logging
    cfg['log_images'] = ['collie64.png', 'gun128.png', 'motor256.png']

    # variable-rate
    cfg['lmb_range'] = (float(lmb_range[0]), float(lmb_range[1]))
    cfg['lmb_embed_dim'] = (256, 256)
    cfg['sin_period'] = 64

    # model configuration
    ch = 128
    enc_dims = [192, ch*3, ch*4, ch*4, ch*4]

    res_block = cm.ConvNeXtBlockAdaLN_fb
    res_block.default_embedding_dim = cfg['lmb_embed_dim'][1]

    im_channels = 3
    cfg['enc_blocks'] = [
        # 64x64
        cm.patch_downsample(im_channels, enc_dims[0], rate=4),
        # 16x16
        *[res_block(enc_dims[0], kernel_size=7) for _ in range(6)],
        res_block(enc_dims[0]),
        cm.wave_downsample(enc_dims[0], enc_dims[1]),
        # 8x8
        *[res_block(enc_dims[1], kernel_size=7) for _ in range(6)],
        cm.SetKey('enc_s8'),
        res_block(enc_dims[1]),
        cm.wave_downsample(enc_dims[1], enc_dims[2]),
        # 4x4
        *[res_block(enc_dims[2], kernel_size=5) for _ in range(6)],
        cm.SetKey('enc_s16'),
        res_block(enc_dims[2]),
        cm.wave_downsample(enc_dims[2], enc_dims[3]),
        # 2x2
        *[res_block(enc_dims[3], kernel_size=3) for _ in range(4)],
        cm.SetKey('enc_s32'),
        res_block(enc_dims[3]),
        cm.wave_downsample(enc_dims[3], enc_dims[4]),
        # 1x1
        *[res_block(enc_dims[4], kernel_size=1) for _ in range(4)],
        cm.SetKey('enc_s64'),
    ]

    dec_dims = [ch*4, ch*4, ch*3, ch*2, ch*1]
    z_dims = [32, 32, 96, 8]
    cfg['dec_blocks'] = [
        # 1x1
        *[blk.VRLVBlockV2_base(dec_dims[0], z_dims[0], enc_key='enc_s64', enc_width=enc_dims[-1], kernel_size=1, mlp_ratio=4,
                         name='dec_s64') for _ in range(1)],
        res_block(dec_dims[0], kernel_size=1, mlp_ratio=4),
        cm.wave_upsample(dec_dims[0], dec_dims[1], rate=2),
        # 2x2
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        blk.CrossAttnTransformerNCHW_(dec_dims[1], kv_name='dec_s64', kv_dim=dec_dims[0]),
        *[blk.VRLVBlockV2_base(dec_dims[1], z_dims[1], enc_key='enc_s32', enc_width=enc_dims[-2], kernel_size=3, mlp_ratio=3,
                        name='dec_s32') for _ in range(2)],
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        cm.wave_upsample(dec_dims[1], dec_dims[2], rate=2),
        # 4x4
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        blk.CrossAttnTransformerNCHW_(dec_dims[2], kv_name='dec_s32', kv_dim=dec_dims[1]),
        *[blk.VRLVBlockV2_base(dec_dims[2], z_dims[2], enc_key='enc_s16', enc_width=enc_dims[-3], kernel_size=5, mlp_ratio=2,
                         name='dec_s16') for _ in range(3)],
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        cm.wave_upsample(dec_dims[2], dec_dims[3], rate=2),
        # 8x8
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        blk.CrossAttnTransformerNCHW_(dec_dims[3], kv_name='dec_s16', kv_dim=dec_dims[2]),
        *[blk.VRLVBlockV2_base(dec_dims[3], z_dims[3], enc_key='enc_s8', enc_width=enc_dims[-4], kernel_size=7, mlp_ratio=1.75,
                         name='dec_s8') for _ in range(3)],
        cm.CompresionStopFlag(), # no need to execute remaining blocks when compressing
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        cm.wave_upsample(dec_dims[3], dec_dims[4], rate=2),
        # 16x16
        *[res_block(dec_dims[4], kernel_size=7, mlp_ratio=1.5) for _ in range(8)],
        cm.patch_upsample(dec_dims[4], im_channels, rate=4)
    ]

    model = blk.VariableRateLossyVAE(cfg)

    if pretrained is True:
        pretrained = ''
        msd = torch.load(pretrained)['model']
        model.load_state_dict(msd)
        raise NotImplementedError()
        # url = ''
        # msd = load_state_dict_from_url(url)['model']
        # model.load_state_dict(msd)
    elif pretrained: # str or Path
        msd = torch.load(pretrained)['model']
        model.load_state_dict(msd)
    return model


@register_model
def BG_VAE(lmb_range=(64,8192), pretrained=False):
    cfg = dict()
    #to determine the Z block resblock type

    # maximum downsampling factor
    cfg['max_stride'] = 64
    # images used during training for logging
    cfg['log_images'] = ['collie64.png', 'gun128.png', 'motor256.png']

    # variable-rate
    cfg['lmb_range'] = (float(lmb_range[0]), float(lmb_range[1]))
    cfg['lmb_embed_dim'] = (256, 256)
    cfg['sin_period'] = 64

    # model configuration
    ch = 128
    enc_dims = [192, ch*3, ch*4, ch*4, ch*4]

    res_block = cm.ConvNeXtBlockAdaLN_fb
    res_block.default_embedding_dim = cfg['lmb_embed_dim'][1]

    im_channels = 3
    cfg['enc_blocks'] = [
        # 64x64
        cm.patch_downsample(im_channels, enc_dims[0], rate=4),
        # 16x16
        *[res_block(enc_dims[0], kernel_size=7) for _ in range(6)],
        res_block(enc_dims[0]),
        cm.wave_downsample(enc_dims[0], enc_dims[1]),
        # 8x8
        *[res_block(enc_dims[1], kernel_size=7) for _ in range(6)],
        cm.SetKey('enc_s8'),
        res_block(enc_dims[1]),
        cm.wave_downsample(enc_dims[1], enc_dims[2]),
        # 4x4
        *[res_block(enc_dims[2], kernel_size=5) for _ in range(6)],
        cm.SetKey('enc_s16'),
        res_block(enc_dims[2]),
        cm.wave_downsample(enc_dims[2], enc_dims[3]),
        # 2x2
        *[res_block(enc_dims[3], kernel_size=3) for _ in range(4)],
        cm.SetKey('enc_s32'),
        res_block(enc_dims[3]),
        cm.wave_downsample(enc_dims[3], enc_dims[4]),
        # 1x1
        *[res_block(enc_dims[4], kernel_size=1) for _ in range(4)],
        cm.SetKey('enc_s64'),
    ]

    dec_dims = [ch*4, ch*4, ch*3, ch*2, ch*1]
    z_dims = [32, 32, 96, 8]
    cfg['dec_blocks'] = [
        # 1x1
        *[blk.VRLVBlockV2_base(dec_dims[0], z_dims[0], enc_key='enc_s64', enc_width=enc_dims[-1], kernel_size=1, mlp_ratio=4,
                         name='dec_s64') for _ in range(1)],
        res_block(dec_dims[0], kernel_size=1, mlp_ratio=4),
        cm.wave_upsample(dec_dims[0], dec_dims[1], rate=2),
        # 2x2
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        blk.CrossAttnTransformerNCHW_(dec_dims[1], kv_name='dec_s64', kv_dim=dec_dims[0]),
        *[blk.VRLVBlockV2_base(dec_dims[1], z_dims[1], enc_key='enc_s32', enc_width=enc_dims[-2], kernel_size=3, mlp_ratio=3,
                        name='dec_s32') for _ in range(2)],
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        cm.wave_upsample(dec_dims[1], dec_dims[2], rate=2),
        # 4x4
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        blk.CrossAttnTransformerNCHW_(dec_dims[2], kv_name='dec_s32', kv_dim=dec_dims[1]),
        *[blk.VRLVBlockV2_base(dec_dims[2], z_dims[2], enc_key='enc_s16', enc_width=enc_dims[-3], kernel_size=5, mlp_ratio=2,
                         name='dec_s16') for _ in range(3)],
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        cm.wave_upsample(dec_dims[2], dec_dims[3], rate=2),
        # 8x8
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        blk.CrossAttnTransformerNCHW_(dec_dims[3], kv_name='dec_s16', kv_dim=dec_dims[2]),
        *[blk.VRLVBlockV2_base(dec_dims[3], z_dims[3], enc_key='enc_s8', enc_width=enc_dims[-4], kernel_size=7, mlp_ratio=1.75,
                         name='dec_s8') for _ in range(3)],
        cm.CompresionStopFlag(), # no need to execute remaining blocks when compressing
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        cm.wave_upsample(dec_dims[3], dec_dims[4], rate=2),
        # 16x16
        *[res_block(dec_dims[4], kernel_size=7, mlp_ratio=1.5) for _ in range(8)],
        cm.patch_upsample(dec_dims[4], im_channels, rate=4)
    ]

    cfg1 = copy.deepcopy(cfg)

    # model configuration
    cfg1['dec_blocks'] = [
        # 1x1
        *[lib.VRLVBlockV2_base(dec_dims[0], z_dims[0], enc_key='enc_s64', enc_width=enc_dims[-1], kernel_size=1, mlp_ratio=4,
                         name='dec_s64') for _ in range(1)],
        res_block(dec_dims[0], kernel_size=1, mlp_ratio=4),
        cm.wave_upsample(dec_dims[0], dec_dims[1], rate=2),
        # 2x2
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        lib.CrossAttnTransformerNCHW_(dec_dims[1], kv_name='dec_s64', kv_dim=dec_dims[0]),
        *[lib.VRLVBlockV2_base(dec_dims[1], z_dims[1], enc_key='enc_s32', enc_width=enc_dims[-2], kernel_size=3, mlp_ratio=3,
                        name='dec_s32') for _ in range(2)],
        res_block(dec_dims[1], kernel_size=3, mlp_ratio=3),
        cm.wave_upsample(dec_dims[1], dec_dims[2], rate=2),
        # 4x4
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        lib.CrossAttnTransformerNCHW_(dec_dims[2], kv_name='dec_s32', kv_dim=dec_dims[1]),
        *[lib.VRLVBlockV2_base(dec_dims[2], z_dims[2], enc_key='enc_s16', enc_width=enc_dims[-3], kernel_size=5, mlp_ratio=2,
                         name='dec_s16') for _ in range(3)],
        res_block(dec_dims[2], kernel_size=5, mlp_ratio=2),
        cm.wave_upsample(dec_dims[2], dec_dims[3], rate=2),
        # 8x8
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        lib.CrossAttnTransformerNCHW_(dec_dims[3], kv_name='dec_s16', kv_dim=dec_dims[2]),
        *[lib.VRLVBlockV2_base(dec_dims[3], z_dims[3], enc_key='enc_s8', enc_width=enc_dims[-4], kernel_size=7, mlp_ratio=1.75,
                         name='dec_s8') for _ in range(3)],
        cm.CompresionStopFlag(), # no need to execute remaining blocks when compressing
        res_block(dec_dims[3], kernel_size=7, mlp_ratio=1.75),
        cm.wave_upsample(dec_dims[3], dec_dims[4], rate=2),
        # 16x16
        *[res_block(dec_dims[4], kernel_size=7, mlp_ratio=1.5) for _ in range(8)],
        cm.patch_upsample(dec_dims[4], im_channels, rate=4)
    ]

    cfg1['pretrained'] = None#specify the path to the pretrained B_VAE model
    
    model = blk.VariableRateLossyVAE_dis_linear_enc_dec_NL_Aff_dual_ensemble(cfg,cfg1)

    if pretrained is True:
        pretrained = ''
        msd = torch.load(pretrained)['model']
        model.load_state_dict(msd,strict=False)
        raise NotImplementedError()
        # url = ''
        # msd = load_state_dict_from_url(url)['model']
        # model.load_state_dict(msd)
    elif pretrained: # str or Path
        msd = torch.load(pretrained)['model']
        model.load_state_dict(msd)
    return model





def main():
    model = P_VAE()
    # print(model)
    num_params = sum([p.numel() for p in model.parameters() if p.requires_grad])
    print(f'Number of parameters: {num_params} ({num_params/1e6:.2f}M)')

    temp_tensor = torch.randn(1, 3, 128, 128)
    #Normalize the input
    temp_tensor = (temp_tensor-temp_tensor.min())/(temp_tensor.max()-temp_tensor.min())
    print(f'Input shape: {temp_tensor.shape}')
    metrics, fdict = model(temp_tensor,return_fdict=True)
    # print(out)
    print(f'Output shape: {fdict["x_hat"].shape}')

if __name__ == '__main__':
    main()