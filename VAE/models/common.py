from collections import OrderedDict
import math
import torch
import torch.nn as nn
import pywt
from torch.autograd import Function
from torch.cuda.amp import autocast
import torch.nn.init as init
def get_conv(in_ch, out_ch, kernel_size, stride, padding, zero_bias=True, zero_weights=False):
    conv = nn.Conv2d(in_ch, out_ch, kernel_size, stride, padding)
    if zero_bias:
        conv.bias.data.mul_(0.0)
    if zero_weights:
        conv.weight.data.mul_(0.0)
    return conv


def conv_k1s1(in_ch, out_ch, zero_bias=True, zero_weights=False):
    return get_conv(in_ch, out_ch, 1, 1, 0, zero_bias, zero_weights)

def conv_k3s1(in_ch, out_ch, zero_bias=True, zero_weights=False):
    return get_conv(in_ch, out_ch, 3, 1, 1, zero_bias, zero_weights)

def conv_k5s1(in_ch, out_ch, zero_bias=True, zero_weights=False):
    return get_conv(in_ch, out_ch, 5, 1, 2, zero_bias, zero_weights)

def conv_k3s2(in_ch, out_ch):
    return get_conv(in_ch, out_ch, kernel_size=3, stride=2, padding=1)

def patch_downsample(in_ch, out_ch, rate=2):
    return get_conv(in_ch, out_ch, kernel_size=rate, stride=rate, padding=0)


def patch_upsample(in_ch, out_ch, rate=2):
    conv = nn.Sequential(
        get_conv(in_ch, out_ch * (rate ** 2), kernel_size=1, stride=1, padding=0),
        nn.PixelShuffle(rate)
    )
    return conv

def deconv(in_ch, out_ch, kernel_size=5, stride=2, zero_weights=False):
    conv = nn.ConvTranspose2d(in_ch, out_ch, kernel_size=kernel_size, stride=stride,
                              output_padding=stride - 1, padding=kernel_size // 2)
    if zero_weights:
        conv.weight.data.mul_(0.0)
    return conv


class ConvNeXtBlockAdaLN_fb(nn.Module):
    default_embedding_dim = 256
    def __init__(self, dim, embed_dim=None, out_dim=None, kernel_size=7, mlp_ratio=2,
                 residual=True, ls_init_value=1e-6):
        super().__init__()
        # depthwise conv
        pad = (kernel_size - 1) // 2
        self.conv_dw = nn.Conv2d(dim, dim, kernel_size=kernel_size, padding=pad, groups=dim)
        # layer norm
        self.norm = nn.LayerNorm(dim, eps=1e-6, elementwise_affine=False)
        self.norm.affine = False # for FLOPs computing
        # AdaLN
        embed_dim = embed_dim or self.default_embedding_dim
        self.embedding_layer = nn.Sequential(
            nn.GELU(),
            nn.Linear(embed_dim, 2*dim),
            nn.Unflatten(1, unflattened_size=(1, 1, 2*dim))
        )
        # MLP
        hidden = int(mlp_ratio * dim)
        out_dim = out_dim or dim
        from timm.layers.mlp import Mlp
        self.mlp = Mlp(dim, hidden_features=hidden, out_features=out_dim, act_layer=nn.GELU)
        # layer scaling
        if ls_init_value >= 0:
            self.gamma = nn.Parameter(torch.full(size=(1, out_dim, 1, 1), fill_value=1e-6))
        else:
            self.gamma = None
        
        self.lamb1 = nn.Parameter(torch.zeros((1,dim,1,1)))
        self.lamb2 = nn.Parameter(torch.zeros((1,dim,1,1)))



        self.residual = residual
        self.requires_embedding = True

    def freq_decompose(self, x):
        x_d = torch.mean(x, (-1,-2), keepdim=True) # [bs, dim,H,W]
        x_h = x - x_d # high freq [bs, ,dim,H,W]
        return x_d, x_h

    def forward(self, x, emb):
        shortcut = x
        # depthwise conv
        x = self.conv_dw(x)
        # print(x.shape)
        #balance the high and low freq
        x_d, x_h = self.freq_decompose(x)
        # print(x_d.shape,x_h.shape)
        x_h = x_h * self.lamb1
        x_d = x_d * self.lamb2
        x = x_d + x_h+x 
        # layer norm
        x = x.permute(0, 2, 3, 1).contiguous()
        x = self.norm(x)
        # AdaLN
        embedding = self.embedding_layer(emb)
        shift, scale = torch.chunk(embedding, chunks=2, dim=-1)
        x = x * (1 + scale) + shift
        # MLP
        x = self.mlp(x)
        x = x.permute(0, 3, 1, 2).contiguous()
        # scaling
        if self.gamma is not None:
            x = x.mul(self.gamma)
        if self.residual:
            x = x + shortcut
        return x


class ConvNeXtBlockLN_fb(nn.Module):
    default_embedding_dim = 256
    def __init__(self, dim, out_dim=None, kernel_size=7, mlp_ratio=2,
                 residual=True, ls_init_value=1e-6,ensemble = False):
        super().__init__()
        # depthwise conv
        pad = (kernel_size - 1) // 2
        self.conv_dw = nn.Conv2d(dim, dim, kernel_size=kernel_size, padding=pad, groups=dim)
        # layer norm
        # self.norm = nn.LayerNorm(dim, eps=1e-6, elementwise_affine=False)
        # self.norm.affine = False # for FLOPs computing
        # AdaLN
        #
        # MLP
        hidden = int(mlp_ratio * dim)
        out_dim = out_dim or dim
        from timm.layers.mlp import Mlp
        self.mlp = Mlp(dim, hidden_features=hidden, out_features=out_dim, act_layer=nn.GELU)
        # layer scaling
        if ls_init_value >= 0:
            self.gamma = nn.Parameter(torch.full(size=(1, out_dim, 1, 1), fill_value=1e-6))
        else:
            self.gamma = None

        self.residual = residual

        self.lamb1 = nn.Parameter(torch.zeros((1, dim, 1, 1)))
        self.lamb2 = nn.Parameter(torch.zeros((1, dim, 1, 1)))

        self.requires_embedding = True
        if ensemble:
            self._init_weights()

    def _init_weights(self):
        # 初始化深度卷积层的权重
        init.kaiming_normal_(self.conv_dw.weight, mode='fan_out', nonlinearity='relu')

        # 如果有偏置，则初始化为0
        if self.conv_dw.bias is not None:
            init.constant_(self.conv_dw.bias, 0)

        # 初始化MLP层的权重和偏置
        for m in self.mlp.modules():
            if isinstance(m, nn.Linear):
                init.xavier_normal_(m.weight)
                if m.bias is not None:
                    init.constant_(m.bias, 0)

    def freq_decompose(self, x):
        x_d = torch.mean(x, (-1,-2), keepdim=True) # [bs, dim,H,W]
        x_h = x - x_d # high freq [bs, ,dim,H,W]
        return x_d, x_h

    def forward(self, x):
        shortcut = x
        # depthwise conv
        x = self.conv_dw(x)

        # balance the high and low freq
        x_d, x_h = self.freq_decompose(x)
        # print(x_d.shape,x_h.shape)
        x_h = x_h * self.lamb1
        x_d = x_d * self.lamb2
        x = x_d + x_h + x

        # layer norm
        x = x.permute(0, 2, 3, 1).contiguous()
        # x = self.norm(x)
        # MLP
        x = self.mlp(x)
        x = x.permute(0, 3, 1, 2).contiguous()
        # scaling
        if self.gamma is not None:
            x = x.mul(self.gamma)
        if self.residual:
            x = x + shortcut
        return x


class ConvNextBlockGroup(nn.Module):
    def __init__(self, configurations):
        super().__init__()
        self.layers = nn.ModuleList()
        for config in configurations:
            sequence = nn.Sequential(
                ConvNeXtBlockLN_fb(config['channels'], config['channels'], kernel_size=config.get('kernel_size', 3), mlp_ratio=4, ensemble=config.get('ensemble', False)),
                ConvNeXtBlockLN_fb(config['channels'], config['channels'], kernel_size=config.get('kernel_size', 3), mlp_ratio=4, ensemble=config.get('ensemble', False))
            )
            self.layers.append(sequence)

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x



class SetKey(nn.Module):
    """ A dummy layer that is used to mark the position of a layer in the network.
    """
    def __init__(self, key):
        super().__init__()
        self.key = key

    def forward(self, x):
        return x


class CompresionStopFlag(nn.Module):
    """ A dummy layer that is used to mark the stop position of encoding bits.
    """
    def __init__(self):
        super().__init__()

    def forward(self, x):
        return x


class FeatureExtracter(nn.Module):
    def __init__(self, blocks):
        super().__init__()
        self.enc_blocks = nn.ModuleList(blocks)

    def forward(self, x):
        features = OrderedDict()
        for i, block in enumerate(self.enc_blocks):
            if isinstance(block, SetKey):
                features[block.key] = x
            else:
                x = block(x)
        return features


class FeatureExtractorWithEmbedding(nn.Module):
    def __init__(self, blocks):
        super().__init__()
        self.enc_blocks = nn.ModuleList(blocks)

    def forward(self, x, emb=None):
        features = OrderedDict()
        for i, block in enumerate(self.enc_blocks):
            if isinstance(block, SetKey):
                features[block.key] = x
            elif getattr(block, 'requires_embedding', False):
                x = block(x, emb)
            else:
                x = block(x)
        return features


class FeatureExtractorWithEmbedding_distill_feature(nn.Module):
    def __init__(self, blocks):
        super().__init__()
        self.enc_blocks = nn.ModuleList(blocks)

    def forward(self, x, emb=None):
        distill_list =[]
        features = OrderedDict()
        for i, block in enumerate(self.enc_blocks):
            if isinstance(block, SetKey):
                features[block.key] = x
            elif getattr(block, 'requires_embedding', False):
                x = block(x, emb)
            else:
                x = block(x)
                # print(block)
                distill_list.append(x)

        return features,distill_list


def sinusoidal_embedding(values: torch.Tensor, dim=256, max_period=64):
    assert values.dim() == 1 and (dim % 2) == 0
    exponents = torch.linspace(0, 1, steps=(dim // 2))
    freqs = torch.pow(max_period, -1.0 * exponents).to(device=values.device)
    args = values.view(-1, 1) * freqs.view(1, dim//2)
    embedding = torch.cat([torch.cos(args), torch.sin(args)], dim=-1)
    return embedding


class Permute(nn.Module):
    def __init__(self, *dims: tuple):
        """ Permute dimensions of a tensor
        """
        super().__init__()
        self.dims = dims

    def forward(self, x):
        return torch.permute(x, dims=self.dims)


class LayerScale(nn.Module):
    def __init__(self, *shape, init_values=1e-5):
        super().__init__()
        self.gamma = nn.Parameter(init_values * torch.ones(*shape))

    def forward(self, x):
        return x * self.gamma


class AdaptiveLayerNorm(nn.Module):
    """ Channel-last LayerNorm with adaptive affine parameters that depend on the \
        input embedding.
    """
    def __init__(self, dim: int, embed_dim: int):
        super().__init__()
        self.dim = dim
        self.layer_norm = nn.LayerNorm(dim, eps=1e-6, elementwise_affine=False)
        self.embedding_layer = nn.Sequential(
            nn.GELU(),
            nn.Linear(embed_dim, 2*dim),
            # nn.Unflatten(dim=1, unflattened_size=(1, 1, 2*dim))
        )
        # TODO: initialize the affine parameters such that the initial transform is identity
        # self.embedding_layer[1].weight.data.mul_(0.01)
        # self.embedding_layer[1].bias.data.fill_(0.0)

    def forward(self, x, emb):
        # x: (B, ..., dim), emb: (B, embed_dim)
        x = self.layer_norm(x)
        scale, shift = self.embedding_layer(emb).chunk(2, dim=1) # (B, dim) x 2
        # (B, dim) -> (B, ..., dim)
        scale = torch.unflatten(scale, dim=1, sizes=[1] * (x.dim() - 2) + [self.dim])
        shift = torch.unflatten(shift, dim=1, sizes=[1] * (x.dim() - 2) + [self.dim])
        x = x * (1 + scale) + shift
        return x







def scaled_dot_product_attention(q: torch.Tensor, k: torch.Tensor, v: torch.Tensor):
    """ A simple implementation of the scaled dot product attention.

    Args:
        q (torch.Tensor): query, shape (..., n1, c1)
        k (torch.Tensor): key, shape (..., n2, c1)
        v (torch.Tensor): value, shape (..., n2, c2)

    Returns:
        torch.Tensor: output, shape (..., n1, c2)
    """
    assert q.shape[:-2] == k.shape[:-2] == v.shape[:-2], f'{q.shape=}, {k.shape=}, {v.shape=}'
    assert (q.shape[-1] == k.shape[-1]) and (k.shape[-2] == v.shape[-2])
    merged = False
    B,N,C = q.shape

    # print(q.shape)
    # print(v.shape)
    # if tokens longer than 256*256， then merge it to batch
    if q.shape[-2] > 128*128 and q.shape[-2] < 200*200:
        q = q.reshape(-1,q.shape[-2]//2,q.shape[-1])
        k = k.reshape(-1,k.shape[-2]//2,k.shape[-1])
        v = v.reshape(-1,v.shape[-2]//2,v.shape[-1])
        merged = True
    elif q.shape[-2] >= 200*200:
        q = q.reshape(-1,q.shape[-2]//4,q.shape[-1])
        k = k.reshape(-1,k.shape[-2]//4,k.shape[-1])
        v = v.reshape(-1,v.shape[-2]//4,v.shape[-1])
        merged = True
    # Batch, ..., N_tokens, C
    attn = q @ k.transpose(-1, -2) / math.sqrt(q.shape[-1])
    attn = torch.softmax(attn, dim=-1)
    # attn = torch.relu(attn)
    out = attn @ v
    # merge back
    if merged:
        out = out.reshape(B,N,C)
    # assert out.shape[-2:] == (q.shape[-2], v.shape[-1])
    return out, attn



class MultiheadAttention(nn.Module):
    def __init__(self, in_dims: int, num_heads: int, attn_dim=None):
        super().__init__()

        if isinstance(in_dims, int):
            q_in, k_in, v_in = in_dims, in_dims, in_dims
        else:
            assert len(in_dims) == 3, f'Invalid {in_dims=}'
            q_in, k_in, v_in = in_dims

        attn_dim = attn_dim or q_in
        assert attn_dim % num_heads == 0, f"{num_heads=} must divide {attn_dim=}."

        self.num_heads = num_heads
        self.q_proj = nn.Linear(q_in, attn_dim)
        self.k_proj = nn.Linear(k_in, attn_dim)
        self.v_proj = nn.Linear(v_in, attn_dim)
        # print(attn_dim)
        self.out_proj = nn.Linear(attn_dim, q_in)

    def split_heads(self, x: torch.Tensor):
        return x.unflatten(-1, sizes=[self.num_heads, -1]).transpose(-2, -3).flatten(0,1)

    def combine_heads(self, x):

        return x.unflatten(0,sizes=[-1,self.num_heads]).transpose(-2, -3).flatten(-2, -1) # (..., N, C)

    def forward(self, q, k, v, return_attn=False):
        assert q.shape[:-2] == k.shape[:-2] == v.shape[:-2], f'{q.shape=}, {k.shape=}, {v.shape=}'
        # Input projections
        q, k, v = self.q_proj(q), self.k_proj(k), self.v_proj(v)
        # Separate into heads
        q, k, v = self.split_heads(q), self.split_heads(k), self.split_heads(v)
        # Attention
        out, attn = scaled_dot_product_attention(q, k, v)
        # print(out.shape)
        # Output
        out = self.combine_heads(out) # (..., N, C)
        # print(out.shape)
        out = self.out_proj(out)
        if return_attn:
            return out, attn
        return out
    

class DWT_Function(Function):
    @staticmethod
    @autocast(enabled=False)
    def forward(ctx, x, w_ll, w_lh, w_hl, w_hh):
        x = x.contiguous()
        ctx.save_for_backward(w_ll, w_lh, w_hl, w_hh)
        ctx.shape = x.shape

        dim = x.shape[1]
        # print(w_ll.shape,w_ll.expand(dim, -1, -1, -1).shape)
        x_ll = torch.nn.functional.conv2d(x, w_ll.expand(dim, -1, -1, -1), stride = 2, groups = dim)
        x_lh = torch.nn.functional.conv2d(x, w_lh.expand(dim, -1, -1, -1), stride = 2, groups = dim)
        x_hl = torch.nn.functional.conv2d(x, w_hl.expand(dim, -1, -1, -1), stride = 2, groups = dim)
        x_hh = torch.nn.functional.conv2d(x, w_hh.expand(dim, -1, -1, -1), stride = 2, groups = dim)
        x = torch.cat([x_ll, x_lh, x_hl, x_hh], dim=1)
        return x

    @staticmethod
    @autocast(enabled=False)
    def backward(ctx, dx):
        if ctx.needs_input_grad[0]:
            w_ll, w_lh, w_hl, w_hh = ctx.saved_tensors
            B, C, H, W = ctx.shape
            dx = dx.view(B, 4, -1, H//2, W//2)

            dx = dx.transpose(1,2).reshape(B, -1, H//2, W//2)
            filters = torch.cat([w_ll, w_lh, w_hl, w_hh], dim=0).repeat(C, 1, 1, 1)
            dx = auto_cast(dx, filters.dtype)
            dx = torch.nn.functional.conv_transpose2d(dx, filters, stride=2, groups=C)

        return dx, None, None, None, None


def auto_cast(tensor, target_type):
    if tensor.dtype != target_type:
        return tensor.to(dtype=target_type)
    return tensor


class IDWT_Function(Function):
    @staticmethod
    @autocast(enabled=False)
    def forward(ctx, x, filters):
        ctx.save_for_backward(filters)
        ctx.shape = x.shape

        B, _, H, W = x.shape
        x = x.view(B, 4, -1, H, W).transpose(1, 2)
        C = x.shape[1]
        x = x.reshape(B, -1, H, W)
        filters = filters.repeat(C, 1, 1, 1)
        x = torch.nn.functional.conv_transpose2d(x, filters, stride=2, groups=C)
        return x

    @staticmethod
    @autocast(enabled=False)
    def backward(ctx, dx):
        if ctx.needs_input_grad[0]:
            filters = ctx.saved_tensors
            filters = filters[0]
            B, C, H, W = ctx.shape
            C = C // 4
            dx = dx.contiguous()
            # dx = auto_cast(dx, filters.dtype)

            w_ll, w_lh, w_hl, w_hh = torch.unbind(filters, dim=0)
            x_ll = torch.nn.functional.conv2d(dx, w_ll.unsqueeze(1).expand(C, -1, -1, -1), stride = 2, groups = C)
            x_lh = torch.nn.functional.conv2d(dx, w_lh.unsqueeze(1).expand(C, -1, -1, -1), stride = 2, groups = C)
            x_hl = torch.nn.functional.conv2d(dx, w_hl.unsqueeze(1).expand(C, -1, -1, -1), stride = 2, groups = C)
            x_hh = torch.nn.functional.conv2d(dx, w_hh.unsqueeze(1).expand(C, -1, -1, -1), stride = 2, groups = C)
            dx = torch.cat([x_ll, x_lh, x_hl, x_hh], dim=1)
        return dx, None

class IDWT_2D(nn.Module):
    def __init__(self, wave):
        super(IDWT_2D, self).__init__()
        w = pywt.Wavelet(wave)
        rec_hi = torch.Tensor(w.rec_hi)
        rec_lo = torch.Tensor(w.rec_lo)
        
        w_ll = rec_lo.unsqueeze(0)*rec_lo.unsqueeze(1)
        w_lh = rec_lo.unsqueeze(0)*rec_hi.unsqueeze(1)
        w_hl = rec_hi.unsqueeze(0)*rec_lo.unsqueeze(1)
        w_hh = rec_hi.unsqueeze(0)*rec_hi.unsqueeze(1)

        w_ll = w_ll.unsqueeze(0).unsqueeze(1)
        w_lh = w_lh.unsqueeze(0).unsqueeze(1)
        w_hl = w_hl.unsqueeze(0).unsqueeze(1)
        w_hh = w_hh.unsqueeze(0).unsqueeze(1)
        filters = torch.cat([w_ll, w_lh, w_hl, w_hh], dim=0)
        self.register_buffer('filters', filters)
        self.filters = self.filters.to(dtype=torch.float32)

    @autocast(enabled=False)
    def forward(self, x):
        # print(self.filters.shape)
        return IDWT_Function.apply(x, self.filters)

class DWT_2D(nn.Module):
    def __init__(self, wave):
        super(DWT_2D, self).__init__()
        w = pywt.Wavelet(wave)
        dec_hi = torch.Tensor(w.dec_hi[::-1]) 
        dec_lo = torch.Tensor(w.dec_lo[::-1])
        # print(dec_hi.shape)


        w_ll = dec_lo.unsqueeze(0)*dec_lo.unsqueeze(1)
        w_lh = dec_lo.unsqueeze(0)*dec_hi.unsqueeze(1)
        w_hl = dec_hi.unsqueeze(0)*dec_lo.unsqueeze(1)
        w_hh = dec_hi.unsqueeze(0)*dec_hi.unsqueeze(1)

        self.register_buffer('w_ll', w_ll.unsqueeze(0).unsqueeze(0))
        self.register_buffer('w_lh', w_lh.unsqueeze(0).unsqueeze(0))
        self.register_buffer('w_hl', w_hl.unsqueeze(0).unsqueeze(0))
        self.register_buffer('w_hh', w_hh.unsqueeze(0).unsqueeze(0))

        self.w_ll = self.w_ll.to(dtype=torch.float32)
        self.w_lh = self.w_lh.to(dtype=torch.float32)
        self.w_hl = self.w_hl.to(dtype=torch.float32)
        self.w_hh = self.w_hh.to(dtype=torch.float32)

    @autocast(enabled=False)
    def forward(self, x):
        return DWT_Function.apply(x, self.w_ll, self.w_lh, self.w_hl, self.w_hh)
    

# patch_downsample
class wave_downsample(nn.Module):
    def __init__(self, channels,outchannels,rate=2):
        super(wave_downsample, self).__init__()
        if rate!=2:
            raise ValueError('rate must be 2')
        self.DWT = DWT_2D('haar')
        self.fuse = nn.Conv2d(channels*4, outchannels,1,1,0)

    def forward(self, x):
        x = self.DWT(x)
        x = self.fuse(x)
        return x



#patch_upsample
class wave_upsample(nn.Module):
    def __init__(self, channels,outchannels,rate=2) :
        super(wave_upsample,self).__init__()
        if rate!=2:
            raise ValueError('rate must be 2')
        
        self.syn = nn.Conv2d(channels,outchannels*4,1,1,0)
        self.IDWT = IDWT_2D('haar')
    
    def forward(self, x):
        x = self.syn(x)
        x = self.IDWT(x)
        return x
    

    

    


    

