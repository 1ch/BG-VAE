from setuptools import setup, find_packages


if __name__ == '__main__':
    packages = find_packages(
        exclude=('images', 'results', 'scripts'),
        include=('VAE')
    )

    setup(
        name='VAE',
        version='0.1',
        description='Lossy image/video compression using variational autoencoders',
        packages=packages,
    )